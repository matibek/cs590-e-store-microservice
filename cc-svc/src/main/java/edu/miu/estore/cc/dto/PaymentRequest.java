package edu.miu.estore.cc.dto;

import java.util.Collection;

import lombok.Data;

@Data
public class PaymentRequest {
    private double price;
    private Collection<CC> payments;
}
