package edu.miu.estore.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CC {
    private String type;
    private String number;
    private String cvv;
}
