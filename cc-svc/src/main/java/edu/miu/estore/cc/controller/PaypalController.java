package edu.miu.estore.cc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.estore.cc.dto.PaymentRequest;
import edu.miu.estore.cc.dto.CC;

@RestController
public class PaypalController {

    private static final Logger logger = LoggerFactory.getLogger(PaypalController.class);

    @PostMapping("/api/pay")
    public boolean createPayment(@RequestBody PaymentRequest request) {
        CC cc = request.getPayments().stream().filter(p -> p.getType().equals("CC")).findFirst().get();
        logger.info(String.format("Made credit card payment[amount=%,.2f, number=%s, cvv=%s]", request.getPrice(), cc.getNumber(), cc.getCvv()));
        return true;
    }
}
