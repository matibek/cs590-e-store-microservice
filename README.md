# E-store : A Microservice Store Application

The application is a microservice of for an online store that sells several products.

It is built using [Spring Boot](http://projects.spring.io/spring-boot/) and is packaged in Docker containers.

## Application Design
![Architecture diagram](./architecture.png "Architecture")

As seen in the image above, the microservices are roughly defined by the function in an e-commerce site. All services communicate using REST over HTTP. This was chosen due to the simplicity of development and testing.

## Installing e-store on Kubernetes

The [deploy folder](./deploy/) contains all the deploy scripts. 

Deploy the e-store application on your k8s cluster using:

```bash
$ kubectl create -f deploy/all.yaml
```

Wait for all the e-store services to start:
```bash
$ kubectl get pods --namespace="e-store"
```

## Uninstall the e-store application
```bash
$ kubectl delete -f deploy/all.yaml
```