package edu.miu.estore.payment.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import edu.miu.estore.payment.dto.PaymentRequest;

@RestController
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    
    @Autowired
    RestTemplate restTemplate;
    
    @PostMapping("/api/pay")
    public boolean createPayment(@RequestBody PaymentRequest request) {
        logger.info("Payment request recieved![paymentType=" + request.getPreferedPaymentType() + "]");
        // Make request to payment adapter service
        String adaptersvcKey = System.getenv(request.getPreferedPaymentType() + "_KEY");
        String adaptersvcEndpoint = System.getenv(request.getPreferedPaymentType() + "_ENDPOINT");
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", adaptersvcKey);
        HttpEntity<Object> req = new HttpEntity<>(request, headers);
        ResponseEntity<Object> response = restTemplate.exchange(adaptersvcEndpoint, HttpMethod.POST, req, Object.class);
        return response.getStatusCode() == HttpStatus.OK;
    }
}
