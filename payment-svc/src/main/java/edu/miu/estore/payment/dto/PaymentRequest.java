package edu.miu.estore.payment.dto;

import java.util.Collection;

import lombok.Data;

@Data
public class PaymentRequest {
    private String preferedPaymentType;
    private Collection<Object> payments;
    private double price;
}
