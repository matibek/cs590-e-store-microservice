package edu.miu.estore.stock.controller;

import edu.miu.estore.stock.dto.StockRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class StockController {

    private static final Logger logger = LoggerFactory.getLogger(StockController.class);

    @PostMapping("/api/stock")
    public boolean refillProduct(@RequestBody StockRequest request) {
        logger.info(String.format("Refill items requested! [product=%s vendor=%s]", request.getName(), request.getVendor()));
        return true;
    }
}
