package edu.miu.estore.stock.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
public class StockRequest {
    private String name;
    private String vendor;
}
