package com.estore.authentication.dto;

import java.util.Set;

import com.estore.authentication.model.Role;

import lombok.Data;

@Data
public class SignupRequest {
    private String username;
    private String password;
    private Set<Role> roles;
    private Object user;
}
