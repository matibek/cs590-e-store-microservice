package com.estore.authentication.controller;

import com.estore.authentication.authentication.JWTTokenProvider;
import com.estore.authentication.dto.JWTResponse;
import com.estore.authentication.dto.LoginDTO;
import com.estore.authentication.dto.SignupRequest;
import com.estore.authentication.dto.UsersvcResponse;
import com.estore.authentication.model.User;
import com.estore.authentication.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class AuthenticateController {

    AuthenticationManager authenticationManager;
    JWTTokenProvider tokenProvider;
    UserService userService;
    Logger logger = LoggerFactory.getLogger(AuthenticateController.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${app.usersvc.url}")
    String usersvcUrl;

    @Value("${app.usersvc.key}")
    String apiKey;

    @Autowired
    public AuthenticateController(AuthenticationManager authenticationManager, JWTTokenProvider tokenProvider,
            UserService userService) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.userService = userService;
    }

    @PostMapping("/signin")
    public ResponseEntity<JWTResponse> authenticateUser(@RequestBody LoginDTO login) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = "";
        try {
            jwt = tokenProvider.generateToken(authentication);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseEntity.ok(new JWTResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> processRegister(@RequestBody SignupRequest user) {
        logger.info("User: " + user);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        // Make request to user service to create the user
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", apiKey);
        HttpEntity<Object> request = new HttpEntity<>(user.getUser(), headers);
        String url = usersvcUrl + "/users";
        UsersvcResponse newUser = restTemplate.postForObject(url, request, UsersvcResponse.class);
        return ResponseEntity.ok(this.userService.addUser(user, newUser.getId()));
    }

}
