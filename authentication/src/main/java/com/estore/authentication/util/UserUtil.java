package com.estore.authentication.util;

import com.estore.authentication.authentication.UserDetail;
import com.estore.authentication.model.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class UserUtil {
    public static UserDetail userToPrincipal(User user) {
        UserDetail userDetail = new UserDetail();
        List<SimpleGrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getRoleName())).collect(Collectors.toList());

        userDetail.setUsername(user.getUsername());
        userDetail.setPassword(user.getPassword());
        userDetail.setAuthorities(authorities);
        userDetail.setUserId(user.getUserId());
        return userDetail;
    }

    public static UserDetail create(User user) {
        return new UserDetail();
    }
}
