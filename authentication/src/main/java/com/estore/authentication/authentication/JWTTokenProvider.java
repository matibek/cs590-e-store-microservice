package com.estore.authentication.authentication;

import io.jsonwebtoken.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JWTTokenProvider implements Serializable {
    @Value("${app.jwt.secret}")
    private String jwtSecret;
    @Value("${app.jwt.jwtExpirationInMs}")
    private int jwtExpirationInMs;
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTTokenProvider.class);

    public String generateToken(Authentication authentication) throws JsonProcessingException {
        UserDetail userDetail = (UserDetail) authentication.getPrincipal();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        ObjectMapper objectMapper = new ObjectMapper();
        String subject = objectMapper.writeValueAsString(userDetail);
        return Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
    public String getUserUsernameFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
        String subject = claims.getSubject();
        ObjectMapper objectMapper = new ObjectMapper();
        UserDetail userDetail;
        try {
            userDetail = objectMapper.readValue(subject, UserDetail.class);
        } catch (JsonProcessingException e) {
            return "empty";
        }
        return userDetail.getUsername();
    }
    public boolean validateToken(String authToken) {
        try {
            Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(authToken);
            return true;
        } catch (SignatureException |
                MalformedJwtException |
                ExpiredJwtException |
                UnsupportedJwtException |
                IllegalArgumentException ex) {
            LOGGER.info(ex.getMessage());
        }
        return false;
    }

}

