package com.estore.authentication.services;

import com.estore.authentication.dto.SignupRequest;
import com.estore.authentication.model.User;
import com.estore.authentication.repository.UserRepository;
import com.estore.authentication.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User NOT Found"));
        return UserUtil.userToPrincipal(user);
    }


    public User addUser(SignupRequest request, long userId) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());
        user.setUserId(userId);
        user.setRoles(request.getRoles());
        return this.userRepository.save(user);
    }
}
