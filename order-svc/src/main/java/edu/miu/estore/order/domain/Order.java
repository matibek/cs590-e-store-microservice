package edu.miu.estore.order.domain;

import java.time.LocalDate;
import java.util.Collection;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "product_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private long userId;
    @ElementCollection
    private Collection<Lineitem> lineitems;
    private LocalDate orderDate;

    public double totalPrice() {
        return lineitems.stream().mapToDouble(l -> l.getPrice()).sum();
    }
}
