package edu.miu.estore.order.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReduceRequest {
    private int quantity;
}
