package edu.miu.estore.order.domain;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class Lineitem {
    private long productId;
    private int quantity;
    private double price;
}
