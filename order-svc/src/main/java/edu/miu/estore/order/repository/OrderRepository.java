package edu.miu.estore.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.miu.estore.order.domain.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    
}
