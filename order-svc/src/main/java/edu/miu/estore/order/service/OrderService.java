package edu.miu.estore.order.service;

import edu.miu.estore.order.domain.Order;

public interface OrderService {
    Order createOrder(Order order);
}
