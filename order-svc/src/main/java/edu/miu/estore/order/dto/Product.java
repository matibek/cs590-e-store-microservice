package edu.miu.estore.order.dto;

import lombok.Data;

@Data
public class Product {
    private int quantity;
    private double price;
}
