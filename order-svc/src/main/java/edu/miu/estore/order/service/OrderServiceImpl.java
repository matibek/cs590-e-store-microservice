package edu.miu.estore.order.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.miu.estore.order.domain.Lineitem;
import edu.miu.estore.order.domain.Order;
import edu.miu.estore.order.dto.PaymentRequest;
import edu.miu.estore.order.dto.Product;
import edu.miu.estore.order.dto.ReduceRequest;
import edu.miu.estore.order.dto.ShippingRequest;
import edu.miu.estore.order.dto.User;
import edu.miu.estore.order.repository.OrderRepository;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    RestTemplate restTemplate;

    @Value("${app.usersvc.key}")
    String usersvcKey;

    @Value("${app.usersvc.url}")
    String usersvcUrl;

    @Value("${app.productsvc.key}")
    String productsvcKey;

    @Value("${app.productsvc.url}")
    String productsvcUrl;

    @Value("${app.paymentsvc.key}")
    String paymentsvcKey;

    @Value("${app.paymentsvc.url}")
    String paymentsvcUrl;

    @Value("${app.shippingsvc.key}")
    String shippingsvcKey;

    @Value("${app.shippingsvc.url}")
    String shippingsvcUrl;

    @Override
    public Order createOrder(Order order) {
        // reduce quantity on product service
        List<Lineitem> lineitems = new ArrayList<>();
        for (Lineitem l: order.getLineitems()) {
            Product product = reduceQuantity(l.getProductId(), l.getQuantity());
            l.setPrice(product.getPrice());
            lineitems.add(l);
        }
        order.setLineitems(lineitems);
        // Get user and send request to payment & shipping
        User user = getUser(order.getUserId());
        sendPayment(user, order.totalPrice());  // Here is a place to do rollback if payment fails
        // Save order
        order.setOrderDate(LocalDate.now());
        Order newOrder = orderRepository.save(order);
        // notify shipping svc
        notifyShipping(user, newOrder.getId());
        return newOrder;
    }

    public User getUser(long userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", usersvcKey);
        HttpEntity<Object> request = new HttpEntity<>(headers);
        String url = usersvcUrl + "/users/" + userId;
        ResponseEntity<User> response = restTemplate.exchange(url, HttpMethod.GET, request, User.class);
        return response.getBody();
    }

    public Product reduceQuantity(long productId, int quantity) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", productsvcKey);
        ReduceRequest body = new ReduceRequest(quantity);
        HttpEntity<Object> request = new HttpEntity<>(body, headers);
        String url = productsvcUrl + "/api/products/" + productId + "/reduce";
        ResponseEntity<Product> response = restTemplate.exchange(url, HttpMethod.POST, request, Product.class);
        return response.getBody();
    }

    public boolean sendPayment(User user, double price) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", paymentsvcKey);
        PaymentRequest body = new PaymentRequest();
        body.setPrice(price);
        body.setPayments(user.getPayments());
        body.setPreferedPaymentType(user.getPreferedPaymentType());
        HttpEntity<Object> request = new HttpEntity<>(body, headers);
        String url = paymentsvcUrl + "/api/pay";
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
        return response.getStatusCode() == HttpStatus.OK;
    }

    public void notifyShipping(User user, long orderId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", shippingsvcKey);
        ShippingRequest body = new ShippingRequest();
        body.setAddress(user.getAddress());
        body.setOrderId(orderId);
        HttpEntity<Object> request = new HttpEntity<>(body, headers);
        String url = shippingsvcUrl + "/api/ship";
        restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
    } 
}
