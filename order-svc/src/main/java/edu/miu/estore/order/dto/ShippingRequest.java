package edu.miu.estore.order.dto;

import lombok.Data;

@Data
public class ShippingRequest {
    private Object address;
    private long orderId;
}
