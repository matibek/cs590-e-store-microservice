package edu.miu.estore.order.security;

import java.util.Collection;

import lombok.Data;

@Data
public class UserDetail {
    private String username;
    private long userId;
    private Collection<Authority> authorities;
}
