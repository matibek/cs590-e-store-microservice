package edu.miu.estore.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.estore.order.domain.Order;
import edu.miu.estore.order.security.UserDetail;
import edu.miu.estore.order.service.OrderService;

@RestController
@RequestMapping(path = "/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    public Order createOrder(Authentication authentication, @RequestBody Order order) {
        UserDetail user = (UserDetail) authentication.getPrincipal();
        order.setUserId(user.getUserId());
        return orderService.createOrder(order);
    }
}
