package edu.miu.estore.order.dto;

import java.util.Collection;

import lombok.Data;

@Data
public class User {
    private Object address;
    private String preferedPaymentType;
    private Collection<Object> payments;
}
