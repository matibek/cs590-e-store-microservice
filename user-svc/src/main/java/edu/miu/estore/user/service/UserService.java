package edu.miu.estore.user.service;

import java.util.List;
import java.util.Optional;

import edu.miu.estore.user.domain.User;

public interface UserService {
    List<User> getAll();
    User create(User user);
    Optional<User> get(long id);
}
