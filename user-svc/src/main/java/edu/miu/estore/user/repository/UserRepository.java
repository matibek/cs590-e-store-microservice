package edu.miu.estore.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.miu.estore.user.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
}
