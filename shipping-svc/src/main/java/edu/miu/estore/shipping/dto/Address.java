package edu.miu.estore.shipping.dto;

import lombok.ToString;
import lombok.Data;

@Data
@ToString
public class Address {
    private String street;
    private String city;
    private String state;
    private String country;
}
