package edu.miu.estore.shipping.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ShippingRequest {
    private Address address;
    private long orderId;
}
