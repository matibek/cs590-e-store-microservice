package edu.miu.estore.shipping.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.estore.shipping.dto.ShippingRequest;

@RestController
public class ShippingController {

    private static final Logger logger = LoggerFactory.getLogger(ShippingController.class);

    @PostMapping("/api/ship")
    public boolean createShipping(@RequestBody ShippingRequest request) {
        logger.info(String.format("Shipping for order created![order_id=%s, address=%s]", request.getOrderId(), request.getAddress()));
        return true;
    }
}
