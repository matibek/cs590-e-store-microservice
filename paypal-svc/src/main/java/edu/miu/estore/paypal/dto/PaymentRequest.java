package edu.miu.estore.paypal.dto;

import java.util.Collection;

import lombok.Data;

@Data
public class PaymentRequest {
    private double price;
    private Collection<Paypal> payments;
}
