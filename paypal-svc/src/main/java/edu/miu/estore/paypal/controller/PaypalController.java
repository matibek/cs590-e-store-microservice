package edu.miu.estore.paypal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.estore.paypal.dto.PaymentRequest;
import edu.miu.estore.paypal.dto.Paypal;

@RestController
public class PaypalController {
    
    private static final Logger logger = LoggerFactory.getLogger(PaypalController.class);
    
    @PostMapping("/api/pay")
    public boolean createPayment(@RequestBody PaymentRequest request) {
        Paypal paypal = request.getPayments().stream().filter(p -> p.getType().equals("PAYPAL")).findFirst().get();
        logger.info(String.format("Made paypal payment[amount=%,.2f, email=%s]", request.getPrice(), paypal.getEmail()));
        return true;
    }
}
