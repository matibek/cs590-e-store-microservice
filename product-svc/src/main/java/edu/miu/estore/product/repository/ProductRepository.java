package edu.miu.estore.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.miu.estore.product.model.Product;
@Repository
public interface ProductRepository  extends JpaRepository<Product,Long> {

    }
