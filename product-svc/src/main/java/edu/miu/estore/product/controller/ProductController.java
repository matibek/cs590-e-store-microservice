package edu.miu.estore.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import edu.miu.estore.product.dto.ReduceRequest;
import edu.miu.estore.product.model.Product;
import edu.miu.estore.product.services.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private ProductService productService;
    
    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    // update Product's quantity
    @PostMapping("/{Id}/reduce")
    public ResponseEntity<Product> reduceQuantity(@RequestBody ReduceRequest request, @PathVariable long Id) {
        return new ResponseEntity<Product>(this.productService.reduceQuantity(request.getQuantity(), Id), HttpStatus.OK);
    }
}