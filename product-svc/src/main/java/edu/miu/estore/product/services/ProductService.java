package edu.miu.estore.product.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.miu.estore.product.model.Product;
import edu.miu.estore.product.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private ProductRepository productRepository;

    @Value("${app.quantity.threshold}")
    private int quantityThreshold;

    @Value("${app.stocksvc.key}")
    String stocksvcKey;

    @Value("${app.stocksvc.url}")
    String stocksvcUrl;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public Product createProduct(Product product) {
        return this.productRepository.save(product);
    }

    public Optional<Product> getProductById(long id) {
        return this.productRepository.findById(id);
    }

    public Product reduceQuantity(Integer orderedQuanity, long Id) {

        Product productUpdated = this.productRepository.findById(Id).get();
        Integer updatedQuantity = productUpdated.getQuantity() - orderedQuanity;
        productUpdated.setQuantity(updatedQuantity);
        this.productRepository.save(productUpdated);
        if (updatedQuantity < quantityThreshold) {
            notifyStock(productUpdated);
        }
        return productUpdated;

    }

    public void notifyStock(Product currentProduct) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", stocksvcKey);
        HttpEntity<Object> request = new HttpEntity<>(currentProduct, headers);
        String url = stocksvcUrl + "/api/stock";
        restTemplate.exchange(url, HttpMethod.POST, request, Boolean.class);
    }
}
