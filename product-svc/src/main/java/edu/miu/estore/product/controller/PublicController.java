package edu.miu.estore.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import edu.miu.estore.product.dto.ReduceRequest;
import edu.miu.estore.product.model.Product;
import edu.miu.estore.product.services.ProductService;

import java.util.List;

@RestController
@RequestMapping("/public/products")
public class PublicController {

    @Autowired
    private ProductService productService;
    
    // get all products
    @GetMapping
    public List<Product> getAllProducts() {
        return productService.getAll();
    }

    // create product
    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }
}