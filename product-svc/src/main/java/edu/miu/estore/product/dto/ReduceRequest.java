package edu.miu.estore.product.dto;

import lombok.Data;

@Data
public class ReduceRequest {
    private int quantity;
}
